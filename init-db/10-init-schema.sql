CREATE TABLE IF NOT EXISTS public.post (
	post varchar NOT NULL, --Uses objectID
	title varchar,
	url varchar,
	author varchar NOT NULL,
	points integer,
	story_text varchar,
	comment_text varchar,
	num_comments integer,
	story_id integer,
	story_title varchar,
	story_url varchar,
	parent_id integer,
	created_at timestamp without time zone NOT NULL,
	created_at_i integer NOT NULL,
	tags varchar[],
	CONSTRAINT pk_post PRIMARY KEY (post)
);

CREATE TABLE IF NOT EXISTS public.user (
	"user" serial NOT NULL,
	username varchar,
	password varchar,
	CONSTRAINT pk_user PRIMARY KEY ("user")
);