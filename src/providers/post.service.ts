import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import Post from '../dto/post.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Post as PostEntity } from '../entities/post.entity';
import { Like } from 'typeorm';
import PaginatedPosts from '../dto/paginated-posts.dto';

@Injectable()
export default class PostsService {

  constructor(
    @InjectRepository(Post)
    private postRepository: Repository<PostEntity>
  ) {}
 
  async getAllPosts(query): Promise<PaginatedPosts> {
    const take = query.items || 5;
    const page = query.page || 1;
    const skip = (page-1) * take ;
    const author = query.author || '';
    const title = query.title || '';
    const tags = query.tags || '';
    const month = query.month || '';

    let queryObj = this.postRepository.createQueryBuilder("post")
    if (author) {
      queryObj.andWhere("lower(post.author) LIKE '%' || lower(:author) || '%'", { author: author })
    }
    if (title) {
      queryObj.andWhere("lower(post.title) LIKE '%' || lower(:title) || '%'", { title: title })
    }
    if (tags) {
      queryObj.andWhere("post.tags @> :tags", { tags: tags ? tags.split(",") : []})
    }
    if (month) {
      queryObj.andWhere("lower(to_char(post.created_at, 'month')) = lower(:month)", { month: month })
    }
                                    
    let [result, count] = await queryObj.take(take).skip(skip).getManyAndCount();

    return { data: result || [], currentPage: Number(page), lastPage: Math.ceil(count / take) || 1, total: count || 0 };
  }
 
  deletePost(id: string) {
    return this.postRepository.delete(id);
  }

  async insertNewPosts(posts: any) {
    const latestPost = await this.postRepository.createQueryBuilder("post")
                                                .select("max(post.post::integer)", "max").getRawOne();

    const filteredPosts = latestPost.max ? posts.filter(p => Number(p.objectID) > Number(latestPost.max)) : posts;
    const finalPosts: Post[] = filteredPosts.map(p => {
      p.post = p.objectID;
      p.tags = p._tags;
      return p;
    });
    
    await this.postRepository.insert(finalPosts);
  }
}