import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import User from '../dto/user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User as UserEntity } from '../entities/user.entity';
import { JwtService } from '@nestjs/jwt';
import * as md5 from 'md5';
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError';

@Injectable()
export default class UserService {

  constructor(
    @InjectRepository(User)
    private userRepository: Repository<UserEntity>,
    private jwtService: JwtService
  ) {}
 
  async authenticate(user: User) {
    const userFound: User = await this.userRepository.findOneOrFail({username: user.username});
    if (userFound && userFound.password == md5(user.password)) {
      const payload = { username: user.username, sub: userFound.user };
      return {
          access_token: this.jwtService.sign(payload),
      };
    }
    throw new EntityNotFoundError(User, 'Password does not match');
  }
}