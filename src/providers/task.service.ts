import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import PostService from './post.service';
import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class TaskService {
  private readonly logger = new Logger(TaskService.name);

  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
    private postService: PostService,
  ) {
    // Runs once at startup, once per hour after that
    this.updatePostsFromExternalApi(true);
  }

  @Cron(CronExpression.EVERY_HOUR)
  async updatePostsFromExternalApi(initial?) {
    const posts = (await this.httpService.get(this.configService.get('EXTERNAL_API_URL')).toPromise());
    if (posts.data) {
      await this.postService.insertNewPosts(posts.data.hits);
    }
    else {
      this.logger.debug('No data returned from external API. Skipping.');  
    }
    if (initial) {
      this.logger.debug('Initial run of external api request completed.');
    }
  }
}
