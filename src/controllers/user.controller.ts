import { Body, Controller, Delete, Get, Post, Param, Query } from '@nestjs/common';
import UserService from '../providers/user.service';
import User from '../dto/user.dto';
import AccessToken from '../dto/access-token.dto';
import { ApiTags } from '@nestjs/swagger';

@Controller('users')
export class UserController {
  constructor(
    private readonly userService: UserService
  ) {}
 
  /**
   * Authenticate your user to get an access token
   */
  @ApiTags('Users')
  @Post('authenticate')
  async authenticate(@Body() user: User): Promise<AccessToken> {
    return await this.userService.authenticate(user);
  }
}