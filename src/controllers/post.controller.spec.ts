import { Test, TestingModule } from '@nestjs/testing';
import { TestDatabaseModule } from '../database/test-database.module';
import * as request from 'supertest';
import { PostModule } from '../repositories/post.module';
import { INestApplication } from '@nestjs/common';

describe('PostController', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [PostModule, TestDatabaseModule],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  describe('/GET posts', () => {
    it(`should return posts`, () => {
      return request(app.getHttpServer())
        .get('/posts')
        .set('Authorization', `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3QiLCJzdWIiOjEsImlhdCI6MTYzMzE5MzAwNCwiZXhwIjoxNjY0NzI5MDA0fQ.QoLyeFb_iIPfjJ19YDNBnC3kA9w214FglwV1blLaYVQ`)
        .expect(200);
    });
  });
  
  describe('/DELETE posts/{post}', () => {
    it(`should delete a post by it's ID`, () => {
      return request(app.getHttpServer())
        .delete('/posts/12345678')
        .set('Authorization', `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3QiLCJzdWIiOjEsImlhdCI6MTYzMzE5MzAwNCwiZXhwIjoxNjY0NzI5MDA0fQ.QoLyeFb_iIPfjJ19YDNBnC3kA9w214FglwV1blLaYVQ`)
        .expect(200);
    });
  });

  afterAll(async () => {
    await app.close();
  });
});
