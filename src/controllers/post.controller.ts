import { Body, Controller, Delete, Get, Param, Query, UseGuards } from '@nestjs/common';
import PostService from '../providers/post.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import Post from '../dto/post.dto';
import PaginatedPosts from '../dto/paginated-posts.dto';
import { ApiTags } from '@nestjs/swagger';
import PostFilterParams from '../dto/post-filter-params.dto';
 
@Controller('posts')
export class PostController {
  constructor(
    private readonly postService: PostService
  ) {}
 
  /**
   * Get all posts paginated and with optional filters
   */ 
  @ApiTags('Posts')
  @UseGuards(JwtAuthGuard)
  @Get()
  async getAllPosts(@Query() query: PostFilterParams): Promise<PaginatedPosts> {
    return await this.postService.getAllPosts(query);
  }
 
  /**
   * Delete a post
   */ 
  @ApiTags('Posts')
  @UseGuards(JwtAuthGuard)
  @Delete(':post')
  async deletePost(@Param('post') post: string) {
    this.postService.deletePost(post);
  }
}