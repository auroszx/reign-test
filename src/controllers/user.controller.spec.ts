import { Test, TestingModule } from '@nestjs/testing';
import { TestDatabaseModule } from '../database/test-database.module';
import * as request from 'supertest';
import { UserModule } from '../repositories/user.module';
import { INestApplication } from '@nestjs/common';
import { EntityNotFoundExceptionFilter } from '../filters/entity-not-found-exception.filter';

describe('PostController', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [UserModule, TestDatabaseModule],
    }).compile();

    app = moduleRef.createNestApplication();
    app.useGlobalFilters(new EntityNotFoundExceptionFilter());
    await app.init();
  });

  describe('/POST users/authenticate (200)', () => {
    it(`should authenticate the user and return an access token`, () => {
      return request(app.getHttpServer())
        .post('/users/authenticate')
        .set('Content-Type', 'application/json')
        .send({username: 'test', password: 'password'})
        .expect(201);
    });
  });

  describe('/POST users/authenticate (404) - User not found', () => {
    it(`should authenticate the user and return an access token`, () => {
      return request(app.getHttpServer())
        .post('/users/authenticate')
        .set('Content-Type', 'application/json')
        .send({username: 'tests', password: 'password'})
        .expect(404);
    });
  });

  describe('/POST users/authenticate (404) - Wrong password', () => {
    it(`should authenticate the user and return an access token`, () => {
      return request(app.getHttpServer())
        .post('/users/authenticate')
        .set('Content-Type', 'application/json')
        .send({username: 'test', password: 'passwords'})
        .expect(404);
    });
  });

  afterAll(async () => {
    await app.close();
  });
});
