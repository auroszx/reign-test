import { ApiHideProperty } from '@nestjs/swagger';

export default class User {
  @ApiHideProperty()
  public user: number;

  public username: string;
 
  public password: string;
}