export default class PostFilterParams {
  /**
   * Number of items per page (default is 5)
   * @example 5
   */ 
  public items?: number;

  /**
   * Page to get (default is 1)
   * @example 3
   */ 
  public page?: number;

  /**
   * Author
   */ 
  public author?: string;


  /**
   * Title
   */ 
  public title?: string;

  /**
   * Tags, comma separated for more than one
   * @example 'ask_hn,story_1234'
   */ 
  public tags?: string;

  /**
   * Month, in text form
   * @example 'september'
   */ 
  public month?: string;
}