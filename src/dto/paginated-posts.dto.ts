import Post from './post.dto';

export default class PaginatedPosts {
  public data: Post[];

  public currentPage: number;

  public lastPage: number;

  public total: number;
}