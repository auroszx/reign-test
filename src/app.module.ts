import { Module } from '@nestjs/common';
import { PostController } from './controllers/post.controller';
import PostService from './providers/post.service';
import { ConfigModule } from '@nestjs/config';
import * as Joi from '@hapi/joi';
import { DatabaseModule } from './database/database.module';
import { PostModule } from './repositories/post.module';
import { UserModule } from './repositories/user.module';
import { ScheduleModule } from '@nestjs/schedule';
import { TaskModule } from './repositories/task.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        POSTGRES_HOST: Joi.string().required(),
        POSTGRES_PORT: Joi.number().required(),
        POSTGRES_USER: Joi.string().required(),
        POSTGRES_PASSWORD: Joi.string().required(),
        POSTGRES_DB: Joi.string().required(),
        PORT: Joi.number(),
      })
    }),
    ScheduleModule.forRoot(),
    DatabaseModule,
    PostModule,
    UserModule,
    TaskModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
