import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
 
@Entity()
export class User {
  @PrimaryGeneratedColumn()
  public user: number;
 
  @Column({unique: true})
  public username: string;

  @Column()
  public password: string;
}