import { Column, Entity, PrimaryColumn } from 'typeorm';
 
@Entity()
export class Post {
  @PrimaryColumn()
  public post: string;
 
  @Column({nullable: true})
  public title: string;

  @Column({nullable: true})
  public url: string;

  @Column()
  public author: string;

  @Column({nullable: true})
  public points: number;

  @Column({nullable: true})
  public story_text: string;

  @Column({nullable: true})
  public comment_text: string;

  @Column({nullable: true})
  public num_comments: number;

  @Column({nullable: true})
  public story_id: number;

  @Column({nullable: true})
  public story_title: string;

  @Column({nullable: true})
  public story_url: string;

  @Column({nullable: true})
  public parent_id: number;

  @Column('timestamp without time zone')
  public created_at: Date;

  @Column({nullable: true})
  public created_at_i: number;

  @Column('varchar', { array: true, nullable: true })
  public tags: string[];
}