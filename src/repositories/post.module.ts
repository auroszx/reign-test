import { Module } from '@nestjs/common';
import { PostController } from '../controllers/post.controller';
import PostService from '../providers/post.service';
import { Post } from '../entities/post.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from '../constants/constants';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from '../auth/jwt.strategy';
 
@Module({
  imports: [
    TypeOrmModule.forFeature([Post]),
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '365 days' },
    }),
    PassportModule,
  ],
  controllers: [PostController],
  providers: [PostService, JwtStrategy],
  exports: [PostService]
})
export class PostModule {}