import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { TaskService } from '../providers/task.service';
import { PostModule } from './post.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [HttpModule, PostModule, ConfigModule],
  providers: [TaskService]
})
export class TaskModule {}