## Description

A test server API that pulls data from an external API every hour for later review.

## Installation

The server is dockerized. To get up and running just do:

```bash
$ docker compose build dev # or prod
$ docker compose up dev # or prod
```

No configuration necessary, this will spin up a postgres image (setup with the required DB, tables) and the server image.

If you want to perform tests, while the container is up run:

```bash
$ npm run test
```

The server will make a request to the external API on startup to populate posts, and once per hour after that.

By default, the app runs on localhost:3005, postgres is accessible at port 5434. This and the api url can be changed in .env.

The default and only user is: test (username), password (password).

API Docs (Swagger) are available at localhost:3005/api/docs.